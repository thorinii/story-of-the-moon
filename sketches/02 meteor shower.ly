% LilyBin
\version "2.18.2"


g_meteorshower_intro_chords = \chordmode {
  f1. |
  f |
  des |
  f |

  f1. |
  des |
  des |
  f2. |

  f2. |
  a |
  a |
}

g_meteorshower_intro_notes = \relative c' {
  \key f \major
  \time 12/8
  \tempo 4. = 75

  f4 a8 a4.~ a4 f8 bes4 bes8 |
  c2. r |
  f,4 c'8 c4.~ c4 f,8 bes4 bes8 |
  a2. r |

  f4 a8 a4.~ a4 f8 bes4 bes8 |
  c2. r |
  des4.~ des8 c8 bes8 ees4. des4. |

  \time 3/4 c2.~ |
  \tempo 4 = 110
  c2. |
  e2.~ |
  e2. |
}

g_meteorshower_intro = <<
  \new ChordNames {
    \set chordChanges = ##t
    \g_meteorshower_intro_chords
  }
  \g_meteorshower_intro_notes
>>


g_meteorshower_waltz_minor_chords = \chordmode {
  d2.:m | d2.:m | d2.:m | d2.:m |
  d2.:m | d2.:m | d2.:m | d2.:m |

  d2.:m | d2.:m |
  c2. | d2.:m |
  d2.:m | d2.:m |
  c2. | d2.:m |

  d2.:m | d2.:m |
  a2.:m | bes2. |
  bes2. | bes2. |
  c2. | c2. |

  d2.:m | d:m |
  c | f |
  d:m | d:m |
  g | d:m |

  bes2. | bes |
  c | d:m |
  bes | f |
  bes | c |
}

g_meteorshower_waltz_minor_notes = \relative c' {
  \key f \major
  \tempo 4 = 110

  \break

  % TODO: waltz
  R2.*8 |

  \break

  d='2 f4 | f2 d4 |
  g2. | a2. |
  d,2 a'4 | a2 d,4 |
  g2. | f2. |

  d='2 f4 | f2 d4 |
  g2. | a2. |
  bes4. a8 g4 | a4 g f |
  g2.~ | g2. |

  \break

  d='2 f4 | f2 d4 |
  g2. | a2. |
  d,2 g4 | a2 c4 |
  b2 g4 | a2. |

  d,='2 f4 | f2 d4 |
  g2. | a2. |
  bes4. a8 g4 | a2 c4 |
  d2. | e2. |
}

g_meteorshower_waltz_minor = <<
  \new ChordNames {
    \set chordChanges = ##t
    \g_meteorshower_waltz_minor_chords
  }
  \g_meteorshower_waltz_minor_notes
>>


g_meteorshower_remainder = \relative c' {
  \break
  \key f \major

  % m52
  c='2\( f4 | f2 c4 |
  g'2. | a\) |
  c,2\( a'4 | a2 c,4 |
  g'2. | f\) |

  c='2\( f4 | f2 c4 |
  g'2. | a\) |
  bes4.\( a8 g4 | a4 g f |
  g2. | g\) |

  c,='2\( f4 | f2 c4 |
  g'4. f8 g4 | a2.\) |
  c,='2\( a'4 | a2 f4 |
  bes4. c8 bes4 | a2.\) |

  f='2\( c'4 | c2 f,4 |
  d'4. e8 d4 | c2.\) |
  des4.\( c8 bes4 | ees2 ees4 |
  f2.~ | f\) |

  % m84
  \break
  e=''2\( e4 | f2 d4 |
  g2 g4 | a2 f4\) |
  bes2\( a4 | g2 bes4 |
  a4. g8 f4 | g2.\) |

  e=''2\( e4 | f2 d4 |
  g2 g4 | a2 f4\) |
  bes2\( bes4 | g2 bes4 |
  a4. g8 f4 | a2 f4\) |

  g2.~ | g~ |
  g~ | g |

  % m104
  \break
  c,=''2.\( | f |
  f | c |
  g'~ | g |
  a~ | a\) |

  c,=''2.\( | f |
  f | c' |
  bes~ | bes |
  a~ | a\) |

  c,=''2.\( | f |
  f | c |
  g'~ | g |
  a~ | a\) |

  c,=''2.\( | g' |
  g | c, |
  c'~ | c |
  d, | e\) |

  f2.~\( | f |
  g~ | g |
  c8\staccato\) r8 r2 | R2. |

  % m142
  \break
  R2.*4

  f,,='2\( a4 | a2 f4 |
  bes2. | c\) |
  f,2\( c'4 | c2 f,4 |
  bes2. | a\) |

  f='2\( a4 | a2 f4 |
  bes2. | c\) |
  c4.\( bes8 a4 | bes2 a4 |
  g2.~ | g\) |

  f='2\( a4 | a2 f4 |
  bes2. | c\) |
  f,2\( c'4 | c2 f,4 |
  bes2. | a\) |

  f='2\( a4 | a2 f4 |
  bes2. | c\) |
  c4.\( bes8 a4 | a4 g f |
  f2.~ | f\) |

  R2.*2 |

  % m180
  \break
  f'=''2.\( | e |
  c | a' |
  g~ | g |
  a~ | a\) |

  f=''2.\( | e |
  c | c' |
  bes~ | bes |
  a~ | a\) |

  c,=''2.\( | f |
  f | c |
  g'~ | g |
  a~ | a\) |

  c,=''2.\( | f |
  f | c' |
  bes~ | bes |
  a~ | a\) |

  d,=''2\( f4 | f2.~ |
  f2 d4 | a'2 g4 |
  g2.~ | g |
  f2. | e2.\) |

  f=''2.~ | f |
  f2.~ | f |
  f2.~ | f~ |
  f2.~ | f |

  % m228
  \break
  f,='2.~ | f |
  e~ | e |

  e='4\( g g | e a a |
  g cis cis | a e' e\) |
  f4.\( e8 d4 | d2 f4 |
  e2.~ | e~ |
  e~ | e\) |

  cis=''2\( e4 | e2 cis4 |
  f2. | g\) |

  % m246
  \break
  a=''2\( a4 | f2 a4 |
  g2 f4 | e2.\) |
  e2\( g4 | g2 e4 |
  f2 e4 | d2.\) |

  d=''2\( f4 | f2 d4 |
  g2. | a\) |
  d,2. | f |
  e~ | e |

  % m262
  \break
  d=''2\( f4 | f2 d4 |
  g2. | a\) |
  d,2\( f4 | f2 d4 |
  g2. | a\) |

  d,=''2 f4 | f2 a,4 |
  d2 f4 | f2 a,4 |
  d2 f4 | f2 a,4 |
  d2.~ | d |
}


g_meteorshower = {
  \g_meteorshower_intro
  \g_meteorshower_waltz_minor
  \g_meteorshower_remainder

  \bar "|."
}


\score {
  \g_meteorshower

  \layout{}
  \midi{}
}
